# Library for Pigeon computers
 
To build library:
```
git clone https://gitlab.com/pigeoncomputers/pigeon-rb-examples.git
cd pigeon-rb-examples/C/library/
chmod u+x makelib
./makelib build
```

To build examples:
```
cd examples
gcc example-rb100.c -o example-rb100 -lpigeonRB -lwiringPi
gcc example-rb300.c -o example-rb300 -lpigeonRB -lwiringPi
gcc example-rb700-advance.c -o example-rb700-advance -lpigeonRB -lwiringPi
```

Note for RB700

Since the libraries use the I2C interface directly, please comment out the following line in the /boot/firmware/config.txt file (or /boot/config.txt in older distributions):

```
dtoverlay=mcp23017,addr=0x20,gpiopin=23
```

To run examples:
```
./example-rb100
./example-rb300
./example-rb700-advance
```