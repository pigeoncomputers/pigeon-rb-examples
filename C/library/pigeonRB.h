/*
 * File:    pigeonRB.h
 *
 * Library for Pigeon computers
 *
 * Copyright (C) 2016 KRISTECH Krzysztof Kajstura
 * www.pigeoncomputers.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 */

#ifndef PIGOENRB_H
#define PIGOENRB_H

#define NUM_DIGITAL_INPUTS 8
#define NUM_DRY_CONTACTS 4
#define NUM_DIGITAL_OUTPUTS 8
#define NUM_LEDS 2

#define MCP23017_BASE 200

typedef struct
{
	int value;
	bool active;
} configElement;

struct modelModelConfig
{
	configElement digitalInputs[NUM_DIGITAL_INPUTS];
	configElement dryContacts[NUM_DRY_CONTACTS];
	configElement digitalOutputs[NUM_DIGITAL_OUTPUTS];
	configElement digitalOutputsOE;
	configElement leds[NUM_LEDS];
	configElement watchdogEnabled;
	configElement watchdogInput;
	configElement ctrl3V3;
	configElement ctrl5V;
	configElement fault5V;
	configElement lanReset;
	configElement mcp23017;
	configElement spi;
};

/*
 *  Pigeon model type
 */
typedef enum
{
	UNKNOWN,
	RB100,
	RB300,
	RB350,
	RB700_ESS,
	RB700_STD,
	RB700_ADV
} model_t;

/*
 *  Functions
 */
extern void pigeonSetup(model_t mod);
extern void pigeonClose(void);
extern bool readOptoInput(uint8_t input_nr);
extern bool readDryInput(uint8_t input_nr);
extern void readBinInputs(bool bi[8], bool di[4]);
extern void writeBinOutput(uint8_t input_nr, bool value);
extern void writeBinOutputs(bool values[8]);
extern void turnOffAllBinOutputs(void);
extern void readAnalogInputs(float ai[4]);
extern void writeAnalogOutputs(uint16_t ao[2]);
extern void readFirmwareVer(uint8_t firm[4]);

#endif /* PIGOENRB_H */
