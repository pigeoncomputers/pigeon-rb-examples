/*
 * File:    pigeonRB.c
 *
 * Library for Pigeon computers
 *
 * Copyright (C) 2017 KRISTECH
 * www.pigeoncomputers.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <wiringPi.h>
#include <stdbool.h>
#include "common.h"
#include "pigeonRB.h"
#include "pigeonSPI.h"
#include <mcp23017.h>

const struct modelModelConfig *pCurrentConfig;

const struct modelModelConfig model100_config = {
	.digitalInputs = {
		{12, 1}, {13, 1}, {18, 1}, {19, 1}, {20, 1}, {21, 1}, {22, 1}, {23, 1}},
	.dryContacts = {{30, 1}, {31, 1}, {32, 1}, {33, 1}},
	.digitalOutputs = {{35, 1}, {36, 1}, {37, 1}, {38, 1}, {39, 1}, {40, 1}, {41, 1}, {42, 1}},
	.digitalOutputsOE = {34, 1},
	.leds = {{45, 1}, {47, 1}},
	.watchdogEnabled = {5, 1},
	.watchdogInput = {44, 1},
	.ctrl3V3 = {28, 1},
	.ctrl5V = {29, 1},
	.fault5V = {43, 1},
	.lanReset = {6, 1},
	.spi = {0, 0},
	.mcp23017 = {0, 0}};

const struct modelModelConfig model300_config = {
	.digitalInputs = {
		{12, 1}, {13, 1}, {18, 1}, {19, 1}, {20, 1}, {21, 1}, {22, 1}, {23, 1}},
	.dryContacts = {{30, 1}, {31, 1}, {32, 1}, {33, 1}},
	.digitalOutputs = {{35, 1}, {36, 1}, {37, 1}, {38, 1}, {39, 1}, {40, 1}, {41, 1}, {42, 1}},
	.digitalOutputsOE = {34, 1},
	.leds = {{45, 1}, {47, 1}},
	.watchdogEnabled = {5, 1},
	.watchdogInput = {44, 1},
	.ctrl3V3 = {28, 1},
	.ctrl5V = {29, 1},
	.fault5V = {43, 1},
	.lanReset = {6, 1},
	.spi = {0, 1},
	.mcp23017 = {0, 0}};

const struct modelModelConfig model700ess_config = {
	.digitalInputs = {
		{MCP23017_BASE + 8, 1}, {MCP23017_BASE + 9, 1}, {MCP23017_BASE + 10, 1}, {MCP23017_BASE + 11, 1}, {MCP23017_BASE + 12, 1}, {MCP23017_BASE + 13, 1}, {MCP23017_BASE + 14, 1}, {MCP23017_BASE + 15, 1}},
	.dryContacts = {{12, 1}, {13, 1}, {16, 1}, {17, 1}},
	.digitalOutputs = {{MCP23017_BASE, 1}, {MCP23017_BASE + 1, 1}, {MCP23017_BASE + 2, 1}, {MCP23017_BASE + 3, 1}, {MCP23017_BASE + 4, 1}, {MCP23017_BASE + 5, 1}, {MCP23017_BASE + 6, 1}, {MCP23017_BASE + 7, 1}},
	.digitalOutputsOE = {0, 0},
	.leds = {{45, 0}, {0, 0}},
	.watchdogEnabled = {5, 0},
	.watchdogInput = {44, 0},
	.ctrl3V3 = {0, 0},
	.ctrl5V = {6, 0},
	.fault5V = {0, 0},
	.lanReset = {0, 0},
	.spi = {0, 0},
	.mcp23017 = {0x20, 1}};

const struct modelModelConfig model700std_config = {
	.digitalInputs = {
		{MCP23017_BASE + 8, 1}, {MCP23017_BASE + 9, 1}, {MCP23017_BASE + 10, 1}, {MCP23017_BASE + 11, 1}, {MCP23017_BASE + 12, 1}, {MCP23017_BASE + 13, 1}, {MCP23017_BASE + 14, 1}, {MCP23017_BASE + 15, 1}},
	.dryContacts = {{12, 1}, {13, 1}, {16, 1}, {17, 1}},
	.digitalOutputs = {{MCP23017_BASE, 1}, {MCP23017_BASE + 1, 1}, {MCP23017_BASE + 2, 1}, {MCP23017_BASE + 3, 1}, {MCP23017_BASE + 4, 1}, {MCP23017_BASE + 5, 1}, {MCP23017_BASE + 6, 1}, {MCP23017_BASE + 7, 1}},
	.digitalOutputsOE = {0, 0},
	.leds = {{45, 0}, {0, 0}},
	.watchdogEnabled = {5, 0},
	.watchdogInput = {44, 0},
	.ctrl3V3 = {0, 0},
	.ctrl5V = {6, 0},
	.fault5V = {0, 0},
	.lanReset = {0, 0},
	.spi = {0, 1},
	.mcp23017 = {0x20, 1}};

const struct modelModelConfig model700adv_config = model700std_config;

static model_t model = UNKNOWN;

/*
 *  Enable/disable all outputs
 *  only for devices where digitalOutputsOE.active = 1
 *  en = true  - enable all outputs
 *  en = false - disable all outputs
 */
void enableOutputs(bool en)
{
	if (pCurrentConfig->digitalOutputsOE.active)
	{
		if (en)
		{
			digitalWrite(pCurrentConfig->digitalOutputsOE.value, 0);
		}
		else
		{
			digitalWrite(pCurrentConfig->digitalOutputsOE.value, 1);
		}
	}
}

/*
 *  pigeonGPIOConfig function
 */
void pigeonGPIOConfig()
{
	int i;

	for (i = 0; i < NUM_DIGITAL_OUTPUTS; i++)
	{
		if (pCurrentConfig->digitalOutputs[i].active)
			pinMode(pCurrentConfig->digitalOutputs[i].value, OUTPUT);
	}

	// binary outputs
	if (pCurrentConfig->digitalOutputsOE.active)
		pinMode(pCurrentConfig->digitalOutputsOE.value, OUTPUT);

	// enable all outputs
	if (pCurrentConfig->digitalOutputsOE.active)
		enableOutputs(true);

	// binary optoisolated inputs
	for (i = 0; i < NUM_DIGITAL_INPUTS; i++)
	{
		if (pCurrentConfig->digitalInputs[i].active)
			pinMode(pCurrentConfig->digitalInputs[i].value, INPUT);
	}

	// dry contact inputs
	for (i = 0; i < NUM_DRY_CONTACTS; i++)
	{
		if (pCurrentConfig->dryContacts[i].active)
			pinMode(pCurrentConfig->dryContacts[i].value, INPUT);
	}
	// watchdog
	if (pCurrentConfig->watchdogEnabled.active)
		pinMode(pCurrentConfig->watchdogEnabled.value, OUTPUT);

	if (pCurrentConfig->watchdogInput.active)
		pinMode(pCurrentConfig->watchdogInput.value, OUTPUT);

	// power control
	if (pCurrentConfig->ctrl3V3.active)
		pinMode(pCurrentConfig->ctrl3V3.value, OUTPUT);

	if (pCurrentConfig->ctrl5V.active)
		pinMode(pCurrentConfig->ctrl5V.value, OUTPUT);

	if (pCurrentConfig->fault5V.active)
		pinMode(pCurrentConfig->fault5V.value, INPUT);

	// LAN active
	if (pCurrentConfig->lanReset.active)
		pinMode(pCurrentConfig->lanReset.value, OUTPUT);

	// LEDs
	for (i = 0; i < NUM_LEDS; i++)
	{
		if (pCurrentConfig->leds[i].active)
			pinMode(pCurrentConfig->leds[i].value, INPUT);
	}
}

/*
 *  pigeonSetup function must be called at the start of program
 */
void pigeonSetup(model_t mod)
{
	int ret_val, i;

	model = mod;

	if (wiringPiSetupGpio() != 0)
	{
		printExit("ERROR: wiringPi setup GPIO\n");
	}

	switch (model)
	{
	case RB100:
		pCurrentConfig = &model100_config;
		break;

	case RB300:
	case RB350:
		pCurrentConfig = &model300_config;
		break;

	case RB700_ESS:
		pCurrentConfig = &model700ess_config;
		break;

	case RB700_STD:
		pCurrentConfig = &model700std_config;
		break;

	case RB700_ADV:
		pCurrentConfig = &model700adv_config;
		break;

	default:
		printExit("wrong model definition\n");
		break;
	}

	if (pCurrentConfig->spi.active)
		spiOpen();

	if (pCurrentConfig->mcp23017.active)
	{
		ret_val = mcp23017Setup(MCP23017_BASE, pCurrentConfig->mcp23017.value);

		if (ret_val == FALSE)
			printExit("ERROR: mcp23017 setup\n");

		for (i = 0; i < 8; i++)
			pinMode(MCP23017_BASE + i, OUTPUT);
	}

	pigeonGPIOConfig();
}

/*
 *  pigeonClose function must be called at the end of program
 */
void pigeonClose(void)
{
	if (pCurrentConfig->spi.active)
		spiClose();
}

/*
 *  Read optoisolated input
 *  input_nr - number of input 1..8
 */
bool readOptoInput(uint8_t input_nr)
{
	return (bool)(1 - digitalRead(pCurrentConfig->digitalInputs[input_nr - 1].value));
}

/*
 *	Read dry contact input
 *  input_nr - number of input 1..4
 */
bool readDryInput(uint8_t input_nr)
{
	return (bool)digitalRead(pCurrentConfig->dryContacts[input_nr - 1].value);
}

/*
 *  Read all binary inputs (optoisolated and dry contact)
 *  bi[8] - values of optoisolated inputs
 *  di[4] - values of dry contact inputs
 */
void readBinInputs(bool bi[8], bool di[4])
{
	int i;

	for (i = 1; i <= 8; i++)
	{
		bi[i - 1] = readOptoInput(i);
	}

	for (i = 1; i <= 4; i++)
	{
		di[i - 1] = readDryInput(i);
	}
}

/*
 *  Write binary output
 *  input_nr - number of output
 *  value - value to write
 */
void writeBinOutput(uint8_t input_nr, bool value)
{
	digitalWrite(pCurrentConfig->digitalOutputs[input_nr - 1].value, value);
}

/*
 *  Write all binary outputs
 *  values[8] - values to write
 */
void writeBinOutputs(bool values[8])
{
	int i;

	for (i = 1; i <= 8; i++)
	{
		writeBinOutput(i, values[i - 1]);
	}
}

/*
 *  Turn off all binary outputs
 */
void turnOffAllBinOutputs(void)
{
	int i;

	enableOutputs(false);
	for (i = 0; i < 8; i++)
	{
		writeBinOutput(pCurrentConfig->digitalOutputs[i].value, false);
	}
	enableOutputs(true);
}

/*
 *  Read of analog inputs
 *  ai[4] - read values
 */
void readAnalogInputs(float ai[4])
{
	int i;
	uint16_t ai_ui[4];

	if (pCurrentConfig->spi.active)
	{
		spiReadAI(ai_ui);
		for (i = 0; i < 4; i++)
		{
			ai[i] = ((float)ai_ui[i] * 10.065) / 1023.0;
		}
	}
	else
	{
		printExit("No analog inputs: wrong model definition\n");
	}
}

/*
 *  Write analog outputs
 *  ao[2] - write values 0 (0V) ... 1000 (10.00V)
 */
void writeAnalogOutputs(uint16_t ao[2])
{
	if (pCurrentConfig->spi.active)
	{
		spiWriteAO(ao);
	}
	else
		printExit("No analog outputs: wrong model definition\n");
}

/*
 *  read firmware version
 */
void readFirmwareVer(uint8_t firm[4])
{
	uint8_t i;

	if (pCurrentConfig->spi.active)
	{
		spiReadFirmwareVer(firm);
	}
	else
	{
		for (i = 0; i < 4; i++)
			firm[i] = 0x00;
	}
}